#include<stdio.h>
#include<string.h>
#include "mpi.h"
int main(int argc,char *argv[])
{
int my_rank;
int numtasks;
int source;
int dest;
int tag=0;
char message[100];
MPI_Status status;
MPI_Init(&argc,&argv);
MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
if(my_rank!=0)
{
sprintf(message,"greetings");
dest=0;
MPI_Send(message,strlen(message)+1,MPI_CHAR,dest,tag,MPI_COMM_WORLD);
}
else
{
for(source=1;source<numtasks;source++)
{
MPI_Recv(message,100,MPI_CHAR,source,tag,MPI_COMM_WORLD,&status);
printf("%s from process %d\n",message,status.MPI_SOURCE);
}

}
MPI_Finalize();

return 0;
}
